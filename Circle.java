public class Circle {
	public static final double PI = 3.1415926535;
	private double diameter;
	private double circumference;
	private double area;
	
	public Circle(double radius) {
		diameter = 2 * radius;
		circumference = 2 * PI * radius;
		area = PI * Math.pow(radius, 2);
	}
	
	public void printDiameter() {
		System.out.printf("The diamter is %22.2f", diameter);
		System.out.println();
	}
	
	public void printCircumference() {
		System.out.printf("The circumference is %16.2f", circumference);
		System.out.println();
	}
	
	public void printArea() {
		System.out.printf("The area is %25.2f", area);
		System.out.println();
	}
}