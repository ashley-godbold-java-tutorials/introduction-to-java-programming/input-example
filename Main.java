import java.util.Scanner;

public class Main {
	
	
	public static void main(String []args){
		//get user input
		Scanner in = new Scanner(System.in);
		System.out.print("Enter the radius of the circle: ");
		double radius = in.nextDouble();
		in.close(); //don't need the scanner anymore
		
		//make the circle and prints its values
		Circle theCircle = new Circle(radius);
		theCircle.printDiameter();
		theCircle.printCircumference();
		theCircle.printArea();
	}
}
